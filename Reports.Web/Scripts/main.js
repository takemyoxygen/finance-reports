﻿requirejs.config({
    // relative url from where modules will load
    baseUrl: "/Scripts/",
    paths: {
        "jquery": "jquery-2.1.1",
        "bootstrap": "bootstrap.min",
        "chosen": "chosen.jquery.min"
    },
    shim: {
        "bootstrap": {
            deps: ["jquery"]
        },
        "chosen": {
            deps: ["jquery"]
        }
    }
});