﻿define(function() {
    return {
        lazy: function(f) {
            var result;
            var hasValue = false;

            return function() {
                if (!hasValue) {
                    result = f.call(this, Array.prototype.slice.call(arguments, 1));
                    hasValue = true;
                }

                return result;
            }
        }
    };
});