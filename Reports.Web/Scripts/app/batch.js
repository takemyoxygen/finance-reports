﻿define(["jquery", "chosen"], function($) {
    
    function date($row) {
        return $row.children(".date").html();
    }

    function amount($row) {
        return $row.children(".amount").html();
    }

    function tags($row) {
        return $row
            .find(".tags select")
            .children(":selected")
            .map(function() {
                return $(this).html();
            })
            .get()
            .join(", ");
    }

    function description($row) {
        return $row.children(".description").html();
    }

    $(function () {

        var $container = $("#batch-expenses-table-container");

        $container.find(".tags select").chosen({
                create_option: true,
                persistent_create_option: true,
                skip_no_results: true
            });

        $container.on("click", "#submit-button", function() {
            var $table = $("#batch-expenses-table");

            var expenses = $table.find("tbody tr")
                .map(function(_, row) {
                    var $row = $(row);
                    return {
                        date: date($row),
                        amount: amount($row),
                        tags: tags($row),
                        description: description($row)
                    }
                })
                .get();

            var url = $table.parents("form").attr("action");
            var data = JSON.stringify(expenses);

            $.ajax({
                url: url,
                type: "POST",
                data: data,
                contentType: "application/json",
                success: function(response) {
                    $container.html(response);
                }
            });

        });

        $container.on("click", ".remove-line", function() {
            $(this).parents("tr").remove();
        });
    });

    return {};
})