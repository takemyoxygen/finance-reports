﻿define(["jquery", "app/dates", "app/loading", "bootstrap"], function($, dates, loading) {
    if (!$) throw "jQuery is not defined :(";

    return {
        init: function(settings) {

            var $start = $("#range-start");
            var $end = $("#range-end");
            var $expensesContainer = $("#expenses-container");
            var $ignoredTagsList = $("#ignored-tags-container ul");
            var $nextMonth = $("#next-month");
            var $prevMonth = $("#prev-month");

            var ignored = [];

            $start.change(refresh);
            $end.change(refresh);

            $prevMonth.click(function() {
                shiftDates(dates.previousMonth, $start);
            });

            $nextMonth.click(function() {
                shiftDates(dates.nextMonth, $end);
            });

            $expensesContainer.on("click", ".expense-tag", function() {
                var text = $(this).siblings(".tag-label").html();
                ignored.push(text);
                createIgnoredTag(text).appendTo($ignoredTagsList);
                refresh();
            });

            $ignoredTagsList.on("click", "a", function() {
                var $link = $(this);
                var index = ignored.indexOf($link.html());
                if (index >= 0) {
                    ignored.splice(index, 1);
                    refresh();
                };
                $link.parent().remove();
            });

            function refresh() {
                return $.ajax({
                    url: settings.endpoint,
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify(createRequest()),
                    success: function(response) {
                        if (response) $expensesContainer.html(response);
                    }
                });
            }

            loading.wrap(refresh)();

            function createIgnoredTag(tagName) {
                var $a = $("<a>")
                    .attr("title", "Click to return the tag back to the report")
                    .html(tagName);

                return $("<li>")
                    .append($a);
            }

            function createRequest() {
                return {
                    range: {
                        start: $start.val(),
                        end: $end.val(),
                    },
                    ignoreTags: ignored
                }
            }

            function shiftDates(newDates, $currentDate) {
                var currentDate = dates.parse($currentDate.val());
                var range = newDates(currentDate);
                $start.val(dates.toInvariantString(range.start));
                $end.val(dates.toInvariantString(range.end));
                refresh();
            }
        }
    };
})