﻿define(function() {
    function withLeadingZeros(string, targetLength) {
        while (string.length < targetLength) {
            string = "0" + string;
        }

        return string;
    }

    return {
        parse: function(string) {
            var parts = string.split('-');
            // new Date(year, month [, day [, hours[, minutes[, seconds[, ms]]]]])
            return new Date(parts[0], parts[1] - 1, parts[2]); // Note: months are 0-based
        },

        toInvariantString: function(date) {
            return date.getFullYear() + "-"
                + withLeadingZeros((date.getMonth() + 1).toString(), 2) + "-"
                + withLeadingZeros(date.getDate().toString(), 2);
        },

        previousMonth: function(date) {
            return {
                start: new Date(date.getFullYear(), date.getMonth() - 1, 1),
                end: new Date(date.getFullYear(), date.getMonth(), 0)
            };
        },

        nextMonth: function(date) {
            return {
                start: new Date(date.getFullYear(), date.getMonth() + 1, 1),
                end: new Date(date.getFullYear(), date.getMonth() + 2, 0),
            }
        }
    };
});