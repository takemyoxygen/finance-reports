﻿define(["jquery", "app/utils"], function($, utils) {

    var loadingModal = utils.lazy(function() {
            return $("#loading-modal");
        });

    function hide() {
         loadingModal().modal("hide");
    }

    function show() {
         loadingModal().modal();
    }

    function wrap(f) {
        return function() {
            show();
            f.apply(this, Array.prototype.slice.call(arguments, 1))
                .then(hide, hide);
        }
    }

    // loading: (T -> Promise) -> (T -> Promise)
    return {
        hide: hide,
        show: show,
        wrap: wrap
    };
});