﻿namespace Reports.Web.Core

open System.Web

module Session =
    
    let memoize<'a> (key: string) f = (fun () ->
        let session = HttpContext.Current.Session
        match session.[key] with
        | o when o <> null -> o :?> 'a
        | _ -> 
            let result = f()
            session.[key] <- result
            result)

    let get<'a> (key: string) = HttpContext.Current.Session.[key] :?> 'a
