﻿namespace Reports.Web.Core.Models.Expenses

open System

type ExpenseModel = {Amount: float; Date: DateTime; Tags: string}

[<CLIMutable>]
type DateRangeModel = {Start: DateTime; End: DateTime}

type ExpensesReportModel  = {Range: DateRangeModel; Expenses: ExpenseModel[]}

type AmountByTagModel = {Amount: float; Tag: string; Percent: float}
type ExpensesByTagReportModel = {Total: float; Amounts: AmountByTagModel[]}

[<CLIMutable>]
type ExpensesRequestModel = { Range: DateRangeModel; IgnoreTags: string[] }