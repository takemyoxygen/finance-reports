﻿namespace Reports.Web.Core.Models.Batch

open System

[<CLIMutable>]
type TextModel = {Text: string}

[<CLIMutable>]
type BatchExpensesSubmitItemModel = {
    Date: string;
    Description: string;
    Amount: float;
    Tags: string;
    Error: string
}

type BatchExpensesSubmitModel = {
    Expenses: BatchExpensesSubmitItemModel[]
    Tags: string[]
}