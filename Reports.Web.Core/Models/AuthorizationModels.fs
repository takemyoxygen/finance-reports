﻿namespace Reports.Web.Core.Models.Authorization

type AuthorizationResultModel = {Token: string; IsSuccessful: bool}