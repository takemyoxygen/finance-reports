﻿namespace Reports.Web.Core

open Reports.Web.Core.Models.Expenses

open System

module Utils =
    
    [<Literal>]
    let InvariantDateFormat = "yyyy-MM-dd"