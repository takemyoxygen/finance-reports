﻿namespace Reports.Web.Core

open System.Configuration
open System.Web

open Reports.DataSource
open Reports.DataSource.Data

module Services =

    let private settings = 
        let appSetting (key: string) ifMissing = 
            let a = ConfigurationManager.AppSettings.[key]
            if a = null then ifMissing
            else a
             
        lazy( { 
                ClientId = appSetting "client_id" Parameters.clientId;
                Secret = appSetting "secret" Parameters.secret } )

    let dataSource() =
        let sessionProvider = (fun () -> Session.get<string> "token")
        Toshl.dataProvider sessionProvider settings