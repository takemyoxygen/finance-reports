﻿namespace Reports.Web.Core

open System
open System.Web.Mvc
open System.Web.Routing

type TokenRequiredAttribute() =
    inherit ActionFilterAttribute()

    override  this.OnActionExecuting(context) = 
        if (context.HttpContext.Session <> null &&
            context.HttpContext.Session.["token"] = null) then
            let route = new RouteValueDictionary()
            route.Add("controller", "Authorization")
            route.Add("action", "Authorize")
            route.Add("returnUrl", context.HttpContext.Request.RawUrl)
            context.Result <- new RedirectToRouteResult(route)
