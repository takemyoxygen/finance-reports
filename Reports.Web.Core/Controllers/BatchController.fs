﻿namespace Reports.Web.Core.Controllers

open System
open System.Web.Mvc

open Reports.Web.Core
open Reports.Web.Core.Models.Batch
open Reports.DataSource
open Reports.DataSource.Data

type BatchController() =
    inherit FsController()

    let toModel (p: ParsedExpense) = {
        BatchExpensesSubmitItemModel.Amount = p.Amount;
        Description = p.Description; 
        Date = match p.Date with
               | Some d -> d.ToString("yyyy-MM-dd")
               | None -> String.Empty; 
        Tags = String.Empty;
        Error = String.Empty}

    [<HttpGet>]
    [<TokenRequired>]
    member this.Index() = this.View();

    [<HttpPost>]
    member this.Index(model: TextModel) = 
        let items = Expenses.parse model.Text
                    |> List.map toModel
                    |> Array.ofList
        let tags = Services.dataSource().Tags() |> Array.map (fun t -> t.Name)

        {Expenses = items; Tags = tags}
        |> this.WithView "Submit"

    [<HttpPost>]
    member this.Submit(expenses: BatchExpensesSubmitItemModel[]) = 
        let dataSource = Services.dataSource()
        let result = expenses 
                     |> Array.map (fun e ->e, {
                                            Expense.Amount = e.Amount; 
                                            Tags = Expenses.tagsFrom e.Tags;
                                            Id = 0;
                                            Date = DateTime.Parse(e.Date)} |> dataSource.Submit)
                     |> Array.filter (fun (_, r) -> match r with | Ok -> false | Error(_) -> true)
                     |> Array.map (fun (e, Error(s)) -> {e with Error = s})
        if result.Length = 0 then this.PartialView "Success" :> ActionResult
        else 
            let tags = Services.dataSource().Tags() |> Array.map (fun t -> t.Name)
            {Expenses = result; Tags = tags}|> this.WithPartial "ExpensesTable" :> ActionResult
