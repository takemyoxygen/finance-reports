﻿namespace Reports.Web.Core.Controllers

open System
open System.Configuration
open System.Web.Mvc

open Reports.Web.Core.Models.Home
open Reports.DataSource

type HomeController() =
    inherit Controller()

    member this.Index() = this.View()

    member this.About() = {Author = "takemyoxygen"} |> this.View
