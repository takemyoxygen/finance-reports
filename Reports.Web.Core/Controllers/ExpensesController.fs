﻿namespace Reports.Web.Core.Controllers

open System
open System.Web.Mvc

open Reports.DataSource
open Reports.Web.Core
open Reports.Web.Core.Models.Expenses
open Reports.DataSource.Data

type ExpensesController() = 
    inherit FsController()

    let dataProvider = Services.dataSource()

    let expenses = (fun () -> dataProvider.Expenses() |> Array.ofSeq) 
                   |> Session.memoize "expenses"

    let groupByTag expenses =
        let byTag, total = expenses |> Expenses.totalByTag 
        
        let amounts = byTag
                      |> Seq.map (fun (tag, amount) -> {Tag = tag; Amount = amount; Percent = amount / total * 100.0})
                      |> Array.ofSeq

        {Total = total; Amounts = amounts}

    [<TokenRequired>]
    member this.Index() =
        let dateFrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)
        let dateEnd = dateFrom.AddMonths(1).AddDays(float -1)

        this.View { Start = dateFrom; End = dateEnd}

    member this.Expenses(request: ExpensesRequestModel) =
        expenses()
        |> Expenses.withinDates request.Range.Start request.Range.End
        |> Expenses.ignoreTags (if request.IgnoreTags = null then [||] else request.IgnoreTags)
        |> groupByTag
        |> this.WithPartial "ExpensesList"