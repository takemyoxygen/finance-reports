﻿namespace Reports.Web.Core.Controllers

open System
open System.Web.Mvc

open Reports.DataSource
open Reports.Web.Core
open Reports.Web.Core.Models.Authorization

type AuthorizationController() =
    inherit Controller()

    let dataProvider = Services.dataSource()

    member this.Success (code: string, state: string) = 
        let token = dataProvider.Authorization.AuthorizationToken state code
        match token with
        | Some(t) -> 
            this.Session.["token"] <- t
            match this.TempData.["returnUrl"] with
            | :? string as url -> this.Redirect(url) :> ActionResult
            | _ -> this.RedirectToAction("Index", "Expenses") :> ActionResult
        | None -> (this.View "Error") :> ActionResult

    member this.Authorize(returnUrl: string) = 
        this.TempData.["returnUrl"] <- returnUrl
        dataProvider.Authorization.AuthorizationUrl()
        |> this.Redirect