﻿namespace Reports.Web.Core.Controllers

open System
open System.Web.Mvc

[<AbstractClass>]
type FsController() =
    inherit Controller()

    member this.WithPartial name model = this.PartialView(name, model)

    member this.WithView (name: string) (model: Object) = this.View(name, model)