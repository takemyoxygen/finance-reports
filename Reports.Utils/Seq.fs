﻿namespace Reports.Utils

module Seq = 
    
    let intersects xs ys = xs |> Seq.exists (fun x -> ys |> Seq.exists ((=) x))
