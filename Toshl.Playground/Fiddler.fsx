﻿#r "..\external\FiddlerCore\FiddlerCore4.dll"

namespace Reporting.Playground.Proxy

open Fiddler
open System

type HttpRequestsStream(traceRequests) =
        
    let e = new Event<string>()
    let triggerEvent = new SessionStateHandler(fun session ->
        if (traceRequests) then printfn "Request to %s" session.fullUrl
        e.Trigger session.fullUrl)
    do 
        FiddlerApplication.add_BeforeRequest triggerEvent
        printfn "starting proxy"
        FiddlerApplication.Startup(0, FiddlerCoreStartupFlags.Default)

    member this.Requests = e.Publish

    interface IDisposable with
        member this.Dispose() = 
            FiddlerApplication.remove_BeforeRequest triggerEvent
            printfn "stopping proxy"
            FiddlerApplication.Shutdown()
