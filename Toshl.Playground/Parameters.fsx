﻿module Reporting.Playground.Parameters.Common

let browser = "iexplore.exe"
let browserArgs url = url

let authorizationEndpoint = "https://toshl.com/oauth2/authorize"
let tokenUrl = "https://toshl.com/oauth2/token"