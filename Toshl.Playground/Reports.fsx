﻿#load "Authorization.fsx"

open System
open System.Runtime.Serialization

open Reporting.Playground.Parameters.Secret

let token = Authorization.authorize()

[<DataContract>]
type ExpenseResponse = {
    [<field: DataMember(Name = "id")>] Id: int;
    [<field: DataMember(Name = "amount")>] Amount: float;
    [<field: DataMember(Name = "date")>] Date: string;
    [<field: DataMember(Name = "tags")>] Tags: string[]
}

type Expense = {
    Amount: float;
    Date: DateTime;
    Tag: string
}

let allExpenses = 
    let page (i: int) = Web.downloadJson<ExpenseResponse[]> "https://api.toshl.com/expenses"  token ["page", i.ToString()]
    let rec loop current = seq {
        let pageContent = page current
        match pageContent with
        | Some(expenses) -> 
            yield! expenses
            yield! loop (current + 1)
        | None -> ()
    }

    loop 1
    |> Seq.collect (fun e -> e.Tags |> Seq.map (fun tag -> {Amount = e.Amount; Date = DateTime.Parse e.Date; Tag= tag}))
    |> Array.ofSeq

let between startDate endDate expenses =
    expenses |> Array.filter(fun e -> e.Date >= startDate && e.Date <= endDate)

let totalByTag expenses =
    expenses
    |> Seq.groupBy (fun e -> e.Tag)
    |> Seq.map (fun (tag, group) -> tag, (group |> Seq.sumBy (fun e -> e.Amount)))

allExpenses
|> between (DateTime(2014, 04, 01)) (DateTime(2014, 04,  10))
|> totalByTag
|> Seq.sortBy(fun (tag, total) -> -total)
|> Seq.iter(fun (tag, total) -> printfn "%s: %f" tag total)
