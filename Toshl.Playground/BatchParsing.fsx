﻿open System
open System.Globalization

let culture = "nl-nl"

let amount (input: string) = 
    let noSpaces = input.Replace(" ", String.Empty)
    let mutable result = 0.0
    if Double.TryParse(noSpaces, NumberStyles.Any, new CultureInfo(culture), &result) then Some result
    else None

let date (input: string) = 
    let formatted = input.Replace("`", String.Empty)
    let mutable result = DateTime.MinValue
    if DateTime.TryParse(formatted, new CultureInfo(culture), DateTimeStyles.None, &result) then Some result
    else None

type Item =
    | Date of DateTime option
    | Description of string
    | Amount of float

type Expense = {
    Date: DateTime option;
    Amount: float;
    Description: string
}

let toItem line = 
    match date line with
    | Some d -> Date (Some d)
    | None -> match amount line with
                | Some a -> Amount(a)
                | None -> Description(line)

let toItems (input: string) = 
    input.Split('\n')
    |> Array.map toItem
    |> List.ofArray

let toExpenses items =
    let rec loop date description =
        function
        | (head::tail) -> 
            match head with
            | Date d -> loop d description tail
            | Description s -> loop date ((sprintf "%s %s" description s).Trim()) tail
            | Amount a -> { Date = date; Description = description; Amount = a } :: (loop date String.Empty tail)
        | [] -> []
    loop None String.Empty items

let test = @"24 Jun `14
ABcdef
dddd
NL135 346
- 124,15
NL112262346 2386
test test
KOKOKO
- 1.123,00
23 Jun `14
NS-Naarden Bsm 203 BUSSU,PAS061
- 4,50
Vomar Bussum BUSSUM,PAS061
- 3,73
Nettorama Bussum BUSSUM ,PAS061
- 21,88"

test |> toItems |> toExpenses