﻿#r "System.Runtime.Serialization"

open System
open System.Net
open System.IO
open System.Runtime.Serialization
open System.Runtime.Serialization.Json
open System.Web

let private encode (s: string) = HttpUtility.UrlEncode s

let asQueryString parameters =
    parameters
    |> Seq.map (fun (key, value) -> (encode key)+ "=" + (encode value))
    |> String.concat "&"

let appendToUrl (endpoint: string) parameters = 
    endpoint + "?" + (asQueryString parameters)

let private createClient token (url: string)  =
    let client = WebRequest.Create(url)
    client.Headers.Add("Authorization", "Bearer " + token)
    client

let private getResponse (url: string) token parameters =
    let client = parameters |> appendToUrl url |> createClient token
    client.GetResponse()

let downloadString url token parameters = 
    use response = getResponse url token parameters
    use responseStream = response.GetResponseStream()
    use reader = new StreamReader(responseStream)
    reader.ReadToEnd()

let downloadJson<'a> url token parameters =
    use response = getResponse url token parameters
    use responseStream = response.GetResponseStream()
    try
        DataContractJsonSerializer(typeof<'a>).ReadObject responseStream :?> 'a |> Option.Some
    with
    | _ -> None

let appendToStream (request: StreamWriter) parameters =
    request.Write(asQueryString parameters)

let post (url: string) token (parameters: (string*string) seq) = 
    let client = createClient token url
    client.Method <- "POST"
    client.ContentType <- "application/x-www-form-urlencoded"
    use request = client.GetRequestStream()
    use body = new StreamWriter(request)
    appendToStream body parameters
    body.Flush()
    let resp = client.GetResponse()
    use respStream = resp.GetResponseStream()
    use reader = new StreamReader(respStream)
    reader.ReadToEnd()

let postJson (url: string) token (content: string) = 
    let client = createClient token url
    client.Method <- "POST"
    client.ContentType <- "application/json"
    use request = client.GetRequestStream()
    use body = new StreamWriter(request)
    content |> encode |> body.Write
    body.Flush()
    let resp = client.GetResponse()
    use respStream = resp.GetResponseStream()
    use reader = new StreamReader(respStream)
    reader.ReadToEnd()
