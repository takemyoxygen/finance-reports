﻿#load "Authorization.fsx"

let endpoint = "https://api.toshl.com/expenses"

let token = Authorization.authorize()
let incomes = Web.downloadString endpoint token Seq.empty

let addExpense (amount:float) month day =

    ["amount", "100.00";
     "date", sprintf "2014-%02i-%02i" month day;
     "tags[]", "test";
     "tags[]", "more"
     ]
    
    //"{\"amount\":\"100.00\",\"date\":\"2014-05-06\",\"tags\":[\"test\"]}"
    |> Web.post endpoint token


addExpense 1000.0 6 6