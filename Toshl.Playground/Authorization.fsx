﻿#load "Fiddler.fsx"
#load "Parameters.fsx"
#load "SecretParameters.fsx"
#load "Web.fsx"

#r "System.Runtime.Serialization"

open System
open System.Net
open System.Text
open System.IO
open System.Diagnostics
open System.Runtime.Serialization
open System.Runtime.Serialization.Json

open Reporting.Playground.Proxy
open Reporting.Playground.Parameters.Common
open Reporting.Playground.Parameters.Secret

[<DataContract>]
type TokenResponse = { [<field: DataMember(Name="access_token") >] Token: string}

let extractQueryString (url: string) =
    let asTuple (array: 'a[]) = array.[0], array.[1]
    match (url.IndexOf "?") with
    | -1 -> Map.empty<string, string>
    | i -> url.Substring(i + 1).Split('&') 
            |> Array.map (fun keyValue -> keyValue.Split('=') |> asTuple)
            |> Map.ofSeq

let getAuthorizationCode() = async {
    
    let authorizationUrl = 
        ["client_id", clientId; 
        "response_type", "code";
        "state", state] 
        |> Web.appendToUrl authorizationEndpoint

    use stream = new HttpRequestsStream(false)
    // the only thing that you need to do maually - authenticate on Toshl website
    let browser = Process.Start(browser, browserArgs authorizationUrl)
    let! url = stream.Requests
                |> Event.filter (fun url -> url.StartsWith redirectUrl && url.Contains("code="))
                |> Async.AwaitEvent
    printfn "Full redirect URL: %s" url
    browser.Kill()
    return url |> extractQueryString |> Map.find "code"
}

let getToken code = 
    let request = WebRequest.Create(tokenUrl)
    let encoded = Convert.ToBase64String(Encoding.UTF8.GetBytes(clientId + ":" + secret));
    request.Headers.Add("Authorization", "Basic " + encoded)
    request.Method <- "POST"
    request.ContentType <- "application/x-www-form-urlencoded"
    use writer = new StreamWriter(request.GetRequestStream())
    ["grant_type", "authorization_code";
    "code", code] |> Web.appendToStream writer
    writer.Flush()
    let tokenResponse = request.GetResponse().GetResponseStream()
                        |> DataContractJsonSerializer(typeof<TokenResponse>).ReadObject
                        :?> TokenResponse
    tokenResponse.Token

let authorize() = 
    let code = getAuthorizationCode() |> Async.RunSynchronously
    let token = getToken code
    printfn "Token: %s" token
    token