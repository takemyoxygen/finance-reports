﻿namespace Reports.DataSource

open System.Net
open System.IO

open Reports.DataSource.Responses

module Authorization =

    let authorizationUrl clientId state = 
         ["client_id", clientId; 
        "response_type", "code";
        "state", state] 
        |> Web.appendToUrl Parameters.authorizationEndpoint
    
    let getToken clientId secret code state =
        let request = WebRequest.Create Parameters.tokenEndpoint
                      |> Web.withBasicCredentials clientId secret
        request.Method <- "POST"
        request.ContentType <- "application/x-www-form-urlencoded"
        use writer = new StreamWriter(request.GetRequestStream())

        ["grant_type", "authorization_code";
        "code", code] |> Web.appendToBody writer
        writer.Flush()

        use response = request.GetResponse()
        use responseStream = response.GetResponseStream()
        responseStream
        |> Web.deserializeJson<TokenResponse>
        |> Option.bind (fun resp -> Some(resp.Token))
        