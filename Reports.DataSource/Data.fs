﻿namespace Reports.DataSource.Data

open System

type Expense = {
    Id: int;
    Amount: float;
    Tags: string[];
    Date: DateTime;
}

type ParsedExpense = {
    Amount: float;
    Description: string
    Date: DateTime option
}

type Result =
    | Ok
    | Error of string

type Tag = {
    Id: int;
    Name: string;}