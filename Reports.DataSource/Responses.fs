﻿namespace Reports.DataSource.Responses

open System.Runtime.Serialization

[<DataContract>]
type TokenResponse = { [<field: DataMember(Name="access_token") >] Token: string}

[<DataContract>]
type ExpenseResponse = {
    [<field: DataMember(Name = "id")>] Id: int;
    [<field: DataMember(Name = "amount")>] Amount: float;
    [<field: DataMember(Name = "date")>] Date: string;
    [<field: DataMember(Name = "tags")>] Tags: string[]
}

[<DataContract>]
type TagResponse = {
    [<field: DataMember(Name = "id")>] Id: int;
    [<field: DataMember(Name = "name")>] Name: string;
    [<field: DataMember(Name = "count")>] Count: int
}