﻿namespace Reports.DataSource

open System

open Reports.DataSource.Data

type ApiSettings = {
    ClientId: string;
    Secret: string
}

type AuthorizationProvider =
    abstract member AuthorizationUrl: unit -> string
    abstract member AuthorizationToken: state: string -> code: string -> string option

type DataProvider = 
    abstract Authorization: AuthorizationProvider
    abstract Expenses: unit -> Expense seq
    abstract Submit: Expense -> Result
    abstract Tags: unit -> Tag[]