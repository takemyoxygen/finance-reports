﻿namespace Reports.DataSource

open System
open System.Globalization

open Reports.DataSource.Responses
open Reports.DataSource.Data
open Reports.Utils

module Expenses =
    
    let private endpoint = "https://api.toshl.com/expenses"
    let private tagsEndpont = "https://api.toshl.com/expenses/tags"

    let private downloadPaged<'a> endpoint token =
        let page (i: int) = Web.downloadJson<'a[]> endpoint  token ["page", i.ToString()]
        let rec loop current = seq {
            let pageContent = page current
            match pageContent with
            | Some(expenses) -> 
                yield! expenses
                yield! loop (current + 1)
            | None -> ()
        }

        loop 1


    let all token = 
        let page (i: int) = Web.downloadJson<ExpenseResponse[]> endpoint  token ["page", i.ToString()]
        let rec loop current = seq {
            let pageContent = page current
            match pageContent with
            | Some(expenses) -> 
                yield! expenses
                yield! loop (current + 1)
            | None -> ()
        }

        loop 1
        |> Seq.map (fun r -> {Expense.Id = r.Id; Amount = r.Amount; Tags = r.Tags; Date = DateTime.Parse r.Date})

    let withinDates dateFrom dateTo (expenses: Expense seq)  = 
        expenses |> Seq.filter (fun e -> e.Date >= dateFrom && e.Date <= dateTo)

    let totalByTag (expenses: Expense seq) =
        let total = expenses |> Seq.sumBy (fun e -> e.Amount)

        let byTag = expenses
                    |> Seq.collect (fun e -> e.Tags |> Seq.map (fun tag -> e.Amount, tag))
                    |> Seq.groupBy snd
                    |> Seq.map (fun (tag, group) -> tag, (group |> Seq.sumBy fst))
                    |> Seq.sortBy (fun (_, amount) -> -amount)

        byTag, total

    let ignoreTags tags = Seq.filter (fun e -> e.Tags |> Seq.intersects tags |> not)

    type private Item =
        | Date of DateTime option
        | Description of string
        | Amount of float


    let parse (input: string) = 

        let culture = "nl-nl"

        let amount (input: string) = 
            let noSpaces = input.Replace(" ", String.Empty)
            let mutable result = 0.0
            if Double.TryParse(noSpaces, NumberStyles.Any, new CultureInfo(culture), &result) then Some result
            else None

        let date (input: string) = 
            let formatted = input.Replace("`", String.Empty)
            let mutable result = DateTime.MinValue
            if DateTime.TryParse(formatted, new CultureInfo(culture), DateTimeStyles.None, &result) then Some result
            else None

        let toItem line = 
            match date line with
            | Some d -> Date (Some d)
            | None -> match amount line with
                        | Some a -> Amount(a)
                        | None -> Description(line)

        let toItems (input: string) = 
            input.Split('\n')
            |> Array.map toItem
            |> List.ofArray

        let toExpenses items =
            let rec loop date description =
                function
                | (head::tail) -> 
                    match head with
                    | Date d -> loop d description tail
                    | Description s -> loop date ((sprintf "%s %s" description s).Trim()) tail
                    | Amount a -> { Date = date; Description = description; Amount = a } :: (loop date String.Empty tail)
                | [] -> []
            loop None String.Empty items

        input
        |> toItems
        |> toExpenses
        |> List.filter (fun p -> p.Amount < 0.0)
        |> List.map (fun p -> {p with Amount = abs p.Amount})

    let submit (expense: Expense) token =
        let parameters = seq {
            yield ("amount", expense.Amount.ToString("F2"))
            yield ("date", expense.Date.ToString("yyyy-MM-dd"))
            yield! expense.Tags |> Seq.map (fun t -> "tags[]", t)
        }
        Web.post endpoint token parameters

    let tagsFrom (s: string) = 
        s.Split([|','|], StringSplitOptions.RemoveEmptyEntries)
        |> Array.map (fun tag -> tag.Trim())

    let tags token = 
        downloadPaged<TagResponse> tagsEndpont token
        |> Seq.sortBy (fun t -> -t.Count)
        |> Seq.map (fun t -> {Tag.Id = t.Id; Name = t.Name})
        |> Array.ofSeq