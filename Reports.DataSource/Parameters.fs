﻿namespace Reports.DataSource

module Parameters =
    let clientId = "cbf05aec-97a6-4bd4-ab56-61070b845f69ded2576b17bf34c67d902954872cbd7d"
    let secret = "56d6a5b3-263c-4b37-b13a-e079ff86b0775dc84fb8dfa20b5e621d4065eff6ecbe"

    let authorizationEndpoint = "https://toshl.com/oauth2/authorize"
    let tokenEndpoint = "https://toshl.com/oauth2/token"

    let state = System.Guid.NewGuid().ToString()