﻿namespace Reports.DataSource

module Toshl = 

    let private authorizationProvider (settings: Lazy<ApiSettings>) = {
        new AuthorizationProvider with
            member this.AuthorizationUrl() = 
                Authorization.authorizationUrl settings.Value.ClientId Parameters.state

            member this.AuthorizationToken state code = 
                Authorization.getToken settings.Value.ClientId settings.Value.Secret code state
    }
    
    let dataProvider tokenProvider (settings: Lazy<ApiSettings>) = {
        new DataProvider with 
            member this.Authorization = authorizationProvider(settings)
            member this.Expenses() =
                let token = tokenProvider()
                Expenses.all token
            member this.Submit expense = 
                Expenses.submit expense (tokenProvider())
            member this.Tags() =
                tokenProvider() |> Expenses.tags
        }

