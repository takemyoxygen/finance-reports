﻿namespace Reports.DataSource

module Web =

    open System
    open System.Net
    open System.IO
    open System.Text
    open System.Runtime.Serialization
    open System.Runtime.Serialization.Json
    open System.Web

    open Reports.DataSource.Data

    let private encode (s: string) = HttpUtility.UrlEncode s

    let private createClient token (url: string)  =
        let client = WebRequest.Create(url)
        client.Headers.Add("Authorization", "Bearer " + token)
        client

    let asQueryString parameters =
        parameters
        |> Seq.map (fun (key, value) -> (encode key) + "=" + (encode value))
        |> String.concat "&"

    let appendToStream (request: StreamWriter) parameters =
        request.Write(asQueryString parameters)

    let appendToBody (request: StreamWriter) parameters =
        request.Write(asQueryString parameters)

    let appendToUrl (endpoint: string) parameters = 
        endpoint + "?" + (asQueryString parameters)

    let withBasicCredentials username password (request: WebRequest) =
        let encoded = Convert.ToBase64String(Encoding.UTF8.GetBytes(username + ":" + password));
        request.Headers.Add("Authorization", "Basic " + encoded)
        request

    let private getResponse (url: string) token parameters =
        let client = WebRequest.Create(parameters |> appendToUrl url)
        client.Headers.Add("Authorization", "Bearer " + token)
        client.GetResponse()

    let downloadString url token parameters = 
        use response = getResponse url token parameters
        use responseStream = response.GetResponseStream()
        use reader = new StreamReader(responseStream)
        reader.ReadToEnd()

    let deserializeJson<'a> (stream: Stream) =
        try
            DataContractJsonSerializer(typeof<'a>).ReadObject stream :?> 'a |> Option.Some
        with
        | _ -> None

    let downloadJson<'a> url token parameters =
        use response = getResponse url token parameters
        use responseStream = response.GetResponseStream()
        deserializeJson<'a> responseStream

    let post (url: string) token parameters = 
        let client = createClient token url
        client.Method <- "POST"
        client.ContentType <- "application/x-www-form-urlencoded"
        use request = client.GetRequestStream()
        use body = new StreamWriter(request)
        parameters |> appendToStream body
        body.Flush()
        try
            let resp = client.GetResponse()
            Ok
        with 
            | ex -> Error(ex.Message)
